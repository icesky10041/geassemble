import { Component, OnInit } from '@angular/core';
import { NfcService } from '../nfc-service/nfc.service';
import { Router } from '@angular/router';
import { InterfaceService } from '../interface-service/interface.service';

@Component({
  selector: 'index-board',
  templateUrl: './screen-loading.component.html',
  styleUrls: ['./screen-loading.component.css']
})

export class ScreenLoadingComponent implements OnInit {

  constructor(public service: NfcService, private router: Router, private interfaceService: InterfaceService) {


    setTimeout(() => {
      this.router.navigate(['screen-mother']);
  }, 2000);  //5s
  }


  ngOnInit(): void {

    setTimeout(() => {
      this.router.navigate(['screen-mother']);
  }, 5000);  //5s
  }

  startNfcCardScan() {
    this.service.scanNfcCard(this.getNfcID.bind(this))
  }

  getNfcID(tagId: string) {
    console.log('정명건' + tagId)
  }

  startNfcCardWrite(tagId: string) {
    this.service.writeNfcCard(tagId, this.getResultBooleanType.bind(this))
  }

  getResultBooleanType(tagResult: boolean) {
    console.log('정명건' + tagResult)
  }


}
